{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "# Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The aim of this project is to use demonstrate the use of multivariate analysis techniques (MVA) to classify High Energy Physics (HEP) events. As a practical use case, the performance of several MVA techniques (i.e. optimal rectangular cuts, k-NN, neural networks and BDT) to separate between stop quark and top quark simulated CMS events at 13 TeV will be evaluated and compared."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Within HEP searches, one way to increase the discovery significance of an analysis is to select a phase space dominated by the physical process of interest (i.e. signal) where the contributions from other processes (i.e. backgrounds) are as small as possible. This is usually done by selecting only events that pass certain physically motivated variable cuts. The cuts could be manually designed based on topological or kinematic differences between the final states of the processes, which requires a detailed study and it does not always lead to an optimal classifier. Multivariate analysis techniques take advantage of powerful statistical methods and computing power to build near optimal discrimators from event variables (which are generally referred as features)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this exercise, the goal is to build MVA classifiers that are able to distinguish between events where a pair of stop quarks were produced from top pair production events. For the stop quark, only the decay to of each of the squark to a top quark is going to be considered,a s depicted in the following figure:\n",
    "![Diagram for stop quark pair production](stops2tops.png)\n",
    "Furthermore, only final states with two opposite charged leptons (from the leptonic decay of the $W$ boson) and two jets are going to be considered. This final state is relevant for stop searches because all Standard Model (SM) backgrounds except top pair production are greatly reduced, specially when leptons have different flavour."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Object Definition and Event Selection"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to minimize the misidentification of reconstructed physical objects in the event, some quality criteria is imposed over the muons, electrons, jets and MET of each event. The objects used were reconstructed with the Particle Flow (PF) algorithm. For muons, the [CMS RunII Tight ID](https://twiki.cern.ch/twiki/bin/viewauth/CMS/SWGuideMuonIdRun2) definition together with a minimum $p_T$ of 20 GeV and a relative isolation $ I_{rel}> 0.15$ in a cone of $\\Delta R = 0.4$. Electrons were required to be isolated, pass the Medium ID working point and also to have a minimum $p_T$ of 20 GeV. Only leptons within $|\\eta| < 2.4 $ region were considered. PF jets $E_t$ had to be higher than 30 GeV and pass a [basic ID](https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetID) to reduce fakes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To simplify the analysis, only events with two oppositely charged good leptons of different flavour were selected (i.e. with a muon and an electron). If the event had an additional lepton (with a looser definition) the event was not selected. While in this exercises other backgrounds (e.g. DY and WW) are neglected, to make this exercise comparable with real world analyis, a basic event preselection was also applied. To avoid low energy resonances, the dilepton system invariant mass had to be larger than 20 GeV. Events were also required to have two good jet but no b-tag information was used. The detailed object definition and event selection imposed can be checked at the [analysis code](https://github.com/pablodecm/TopAnalysis_converter/blob/e3b749e05129b6f858d00988abe361197432a7ee/interface/ConverterPHYS14.h) used to create a simplified TTrees."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Samples and MVA variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Monte Carlo (MC) samples used in this produced within the CMS Collaboration for the PHYS14 exercise, whose motivation was to check the analyses readiness for the Run II of the LHC. Newer Run II samples were not available for the stop pair production processes. The MINIAODSIM dataset for each of the samples is:\n",
    "- [/SMS-T2tt_2J_mStop-425_mLSP-325_Tune4C_13TeV-madgraph-tauola/Spring14miniaod-PU20bx25_POSTLS170_V5-v1/MINIAODSIM](https://cmsweb.cern.ch/das/request?input=dataset%3D%2FSMS-T2tt_2J_mStop-425_mLSP-325_Tune4C_13TeV-madgraph-tauola%2FSpring14miniaod-PU20bx25_POSTLS170_V5-v1%2FMINIAODSIM&instance=prod%2Fglobal) - 1045470 events\n",
    "- [/TTJets_MSDecaysCKM_central_Tune4C_13TeV-madgraph-tauola/Spring14miniaod-PU20bx25_POSTLS170_V5-v2/MINIAODSIM\n",
    "](https://cmsweb.cern.ch/das/request?input=dataset%3D%2FTTJets_MSDecaysCKM_central_Tune4C_13TeV-madgraph-tauola%2FSpring14miniaod-PU20bx25_POSTLS170_V5-v2%2FMINIAODSIM&instance=prod%2Fglobal) - 25474122 events\n",
    "\n",
    "The stop MC model was characterized by a $m_{stop} = 450$ GeV and a lightest supersymmetric particle (LSP) mass of 325 GeV. The background sample had much higher statistics than the signal samples, so only part of it was used in this exercise to avoid having very unbalanced training and test samples. Therefore, after preselection the number of simulated stop events is much lower than the fraction of background considered. Stop MC sample statistics is quite low and a larger sample will be desirable in the future for detailed studies. The [TMVA framework](http://arxiv.org/abs/physics/0703039) will be used for the training and evaluation of classifiers in this study. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "# entries T2tt : 3511\n",
      "# entries ttbar : 26830\n"
     ]
    }
   ],
   "source": [
    "from ROOT import TMVA, TFile\n",
    "\n",
    "data_path = \"/gpfs/csic_projects/cms/pablodcm/data/mut/PHYS14_dilepton/\"\n",
    "# signal tree - 425 GeV stop 325 LSP T2tt sample\n",
    "sig_file = TFile(data_path + \"Tree_T2tt_425LSP325.root\")\n",
    "sig_tree = sig_file.Get(\"tree\")\n",
    "print \"# entries T2tt : {}\".format(sig_tree.GetEntries())\n",
    "# background tree - TTbar MadSpin (only part of the sample is used)\n",
    "bkg_file = TFile(data_path + \"Tree_TTJets_MadSpin_0.root\")\n",
    "bkg_tree = bkg_file.Get(\"tree\")\n",
    "print \"# entries ttbar : {}\".format(bkg_tree.GetEntries())\n",
    "# set output file and TMVA factory\n",
    "out_file = TFile(\"stop_multivariate.root\",\"RECREATE\")\n",
    "factory = TMVA.Factory(\"stop_multivariate\",out_file,\n",
    "                       \":\".join([\n",
    "                                \"!V\",\n",
    "                                \"Silent\",\n",
    "                                \"!Color\",\n",
    "                                \"!DrawProgressBar\",\n",
    "                                \"Transformations=I;D;P;G,D\",\n",
    "                                \"AnalysisType=Classification\"]\n",
    "                                ))\n",
    "factory.SetInputTrees(sig_tree, bkg_tree)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "The next step is to choose which variables are going to be provided to the MVA classifiers. In principle, with an complex MVA classifier as Deep Neural Network if enough data and computing power is available low level variables (e.g. four-momenta) [can be solely used to build a powerful discriminant](http://arxiv.org/pdf/1402.4735v2.pdf). However, due to the limited statistics, high level variables features for each event have been based on [previous MVA stop dilepton analysis](http://arxiv.org/abs/1403.4853). A total of six variables are used per event, which are defined as follows:\n",
    "- Missing transverse energy (MET): magnitude of the two-vector $p_T^{miss}$\n",
    "obtained from the negative vector sum of the transverse momenta of all PF reconstructed electrons, jets and muons, and\n",
    "calorimeter energy clusters not associated with any objects.\n",
    "- Dilepton invariant mass ($m_{ll}$):  the invariant mass of the two oppositely charged lepton\n",
    "- Lepton-based stranverse mass ($m_{T2})$: kinematic variable that can be used to measure the masses of pair-produced\n",
    "semi-invisibly decaying heavy particles, further information and calculation procedure is provided [here]( (http://xxx.lanl.gov/abs/hep-ph/0304226).\n",
    "- $\\Delta \\phi_{l-l}$: the polar angular distance between leptons.\n",
    "- $min(\\Delta \\phi_{MET-l_0},\\Delta \\phi_{MET-l_1})$: minimum angular distance between the MET and a lepton.\n",
    "- $min(\\Delta \\phi_{MET-j})$: minimum angular distance between the MET and a good jet."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These variables are added from the samples to the TMVA factory in the following code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Var 0 - Missing Transverse Energy (MET)\n",
    "factory.AddVariable(\"ev_high.met\",\"F\")\n",
    "# Var 1 - Dilepton invariant mass\n",
    "factory.AddVariable(\"ev_high.dilept_inv_mass.\",\"F\")\n",
    "# Var 2 - Dilepton MT2 variable\n",
    "factory.AddVariable(\"ev_high.dilepton_MT2.\",\"F\")\n",
    "# Var 3 - DeltaPhi between leptons\n",
    "factory.AddVariable(\"ev_high.d_phi_l_l.\",\"F\")\n",
    "# Var 4 - Minimum DeltaPhi between MET and lepton\n",
    "factory.AddVariable(\"min(ev_high.d_phi_met_l0,ev_high.d_phi_met_l1)\",\"F\")\n",
    "# Var 5 - Minimum DeltaPhi between MET and a good jet\n",
    "factory.AddVariable(\"ev_high.d_phi_min_met_j.\",\"F\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Stop dilepton cut based analyses are generally based on a MET and a $m_{T2}$ cut to reduce the ttbar background . It is possible that with an near optimal combination of the previous variables a higher signal significance can be achieved."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# MVA Methods"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this section, a brief description of the MVA methods that are going to be used will be provided. The [TMVA Users Guide](http://arxiv.org/pdf/physics/0703039.pdf) includes additional information about the actual implementation and parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Rectangular Cut Optimization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Rectangular Cut Optimization method finds the optimal set of simple window cuts over the features that achieves the best discrimation. Several optimization algorithms can be used, by default it follows a genetic algorithm (GA)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "method_Cuts = factory.BookMethod( TMVA.Types.kCuts, \"Cuts\", \"!H:!V\" );"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## k-Nearest Neighbors (k-NN)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The k-NN method check the categories of the $k$ closest according to a certain metric (called neighbors). The event is then classified by a mayority vote (it can also be a weighted vote by the distance). It has the advantage that only requires computation at classification time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "method_kNN = factory.BookMethod(TMVA.Types.kKNN, \"kNN\", \"!H:!V\" )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Artificial Neural Network (MLP)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An artificial neural network (ANN) is a collection of interconnected neurons, a neuron being a function for a given set of input signals. In particular, a multilayer perceptron (MLP) is a feedforward artificial neural network model that maps sets of input data onto a set of outputs. A MLP consists of multiple layers of nodes in a directed graph, with each layer fully connected to the next one. Except for the input nodes, each node is a processing element (neuron) with a nonlinear activation function. A MLP utilizes a method called backpropagation for training the network (setting the neuron weights). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "method_MLP = factory.BookMethod( TMVA.Types.kMLP, \"MLP_ANN\", \"!H:!V:NeuronType=tanh:VarTransform=N:NCycles=600:HiddenLayers=N+5:TestRate=5:!UseRegulator\" )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Boosted Decision Trees (BDT)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A decision tree is a binary tree-structured classifier: the phase space is split this way into many regions that are\n",
    "eventually classified as signal or background, depending on the majority of training events that end\n",
    "up in the final leaf node. Boosted decision tree extend this concept from one tree to several trees (forest). All trees are derived from the same training ensemble by reweighting events and then combined into a single classifier by a weighted average of the individual decision trees. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "method_BDT = factory.BookMethod(TMVA.Types.kBDT, \"BDT\",\n",
    "                         \":\".join([\n",
    "                         \"!H\",\n",
    "                         \"!V\",\n",
    "                         \"NTrees=850\",\n",
    "                         \"MaxDepth=3\",\n",
    "                         \"BoostType=AdaBoost\",\n",
    "                         \"AdaBoostBeta=0.5\",\n",
    "                         \"SeparationType=GiniIndex\",\n",
    "                         \"nCuts=20\",\n",
    "                         \"PruneMethod=NoPruning\"\n",
    "                         ]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training and Testing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The signal and background samples have been divided in training and testing splits and reweighted automatically by TMVA. We can train, test and evaluate all the previously booked method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "factory.TrainAllMethods()\n",
    "factory.TestAllMethods()\n",
    "factory.EvaluateAllMethods()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, the input variable distributions for signal and background can be directly compared."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "![Input variable distributions for Signal and Background](variables.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The MET, $M_{T2}$ and $min(\\Delta \\phi_{MET-j})$ show clear discriminating power. The TMVA framework is also able to provide a ranking of the variables using the training weights of the MLP and BDT classifiers. As expected, the MET is the most important variable in both cases. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " \n",
    " | Rank |  Variable (ANN MLP)       |  Importance (ANN MLP) | Variable (BDT)            | Importance (BDT)   |\n",
    " |:----:|:-------------------------:|:---------------------:|:-------------------------:|:------------------:|\n",
    " |   1  |        MET                |       2.622e+01       |       MET                 |      2.031e-01     |\n",
    " |   2  |       $m_{ll}$            |       1.209e+01       |     $\\Delta \\phi_{l-l}$   |      1.793e-01     |\n",
    " |   3  |       $M_{T2}$            |       8.176e+00       | $\\Delta\\phi_{MET-l}$      |      1.701e-01     |\n",
    " |   4  | $\\Delta\\phi_{MET-j}$      |       2.799e+00       | $\\Delta\\phi_{MET-j}$      |      1.590e-01     |\n",
    " |   5  | $\\Delta\\phi_{MET-l}$      |       1.542e+00       |         $M_{T2}$          |      1.548e-01     |\n",
    " |   6  |  $\\Delta \\phi_{l-l}$      |       1.406e+00       |         $m_{ll}$          |      1.338e-01     |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another relevant aspect to consider is the linear correlation amongst the input variables, which is shown in the next figure. It is worth mention the high linear correlation between the stransverse mass variable and the $\\Delta \\Phi_{MET-l}$, which is coherent with the $M_{T2}$ variable definition."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Linear correlation between input variables](lin_corr.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to compare the performance of the different classifiers considered, the ROC curves are shown in the following plot. All the classifiers are able to separate signal and background with a similar discriminating power. The MLF artificial neural network is in this case the best classifier overall, followed by the BDT."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![ROC curves for the MVA classifiers considered](ROC_curves.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The corresponding AUC (*Area Under the Curve*) and the signal efficiency at certain working points for each one of the MVA methods used is given at the following table. As expected from the ROC, the MLP is the most performant discriminator. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "| Classifier | Sig. eff @B=0.01 | Sig. eff @B=0.10  | Sig. eff @B=0.30 | AUC   |\n",
    "|:----------:|:----------------:|:-----------------:|:----------------:|:-----:|\n",
    "|  MLP-ANN   |    0.319(11)     |     0.594(11)     |     0.781(09)    | 0.832 |\n",
    "|    BDT     |    0.257(10)     |     0.579(11)     |     0.731(10)    | 0.807 |\n",
    "|    kNN     |    0.330(11)     |     0.576(11)     |     0.730(10)    | 0.799 |\n",
    "|   Cuts     |    0.276(10)     |     0.566(11)     |     0.726(10)    | 0.796 |\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the discriminator distributions are going to be compared for the training and test samples, to check if any of them has been overtrained. For the optimal cuts MVA no discriminator is available (it has a binary output for each working point)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![MLP-ANN output discriminator discribution](MLP_ANN_output.png)\n",
    "![BDT output discriminator discribution](BDT_output.png)\n",
    "![kNN output discriminator discribution](kNN_output.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the discriminator output, it is clear that all classifiers are able to separate signal from background. No clear overfitting is visible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "# Conclusions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Several MVA classifiers have been used to separate stop pair production signal from top pair production background in events with two leptons in the final state. While all the classifiers have shown similar separating capabilities and would outperform a simple cut-based analysis, the MLP neural network has been the most powerfull discriminant overall. No clear overfitting has been detected. Further studies with higher MC statistics and tuning of the MVA method could lead to improved results."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
